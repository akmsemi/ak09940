#ifndef AK09940_H
#define AK09940_H

#include "mbed.h"
#include "akmecompass.h"

// REGISTER MAP
/* AK09940 Register Addresses */
#define AK09940_REG_WIA1               0x00 // Company Name (data=48h)
#define AK09940_REG_WIA2               0x01 // Device Code (data=A0h)
#define AK09940_REG_RSV1               0x02 // INFO[7:4]Fab.info,[3:0]Revision(1h)
#define AK09940_REG_RSV2               0x03 // For expansion
#define AK09940_REG_ST1                0x10 // FNUM[3:0],DRDY
#define AK09940_REG_HXL                0x11 // HX[7:0]
#define AK09940_REG_HXM                0x12 // HX[15:8]
#define AK09940_REG_HXH                0x13 // HX[17:16]
#define AK09940_REG_HYL                0x14 // HY[7:0]
#define AK09940_REG_HYM                0x15 // HY[15:8]
#define AK09940_REG_HYH                0x16 // HY[17:16]
#define AK09940_REG_HZL                0x17 // HZ[7:0]
#define AK09940_REG_HZM                0x18 // HZ[15:8]
#define AK09940_REG_HZH                0x19 // HZ[17:16]
#define AK09940_REG_TMPS               0x1A // TMPS[7:0]
#define AK09940_REG_ST2                0x1B // INV(Invalid data),DOR
#define AK09940_REG_CNTL1              0x30 // WM[2:0]
#define AK09940_REG_CNTL2              0x31 // TMP
#define AK09940_REG_CNTL3              0x32 // FIFO,MT[1:0],MODE[4:0]
#define AK09940_REG_CNTL4              0x33 // SRST
#define AK09940_REG_I2CDIS             0x36 // I2C_DISABLE

#define AK09940_MODE_POWERDOWN         0x00 // POWER DOWN MODE
#define AK09940_MODE_SNG_MEASURE       0x01 // SINGLE MEASUREMENT MODE
#define AK09940_MODE_CONT1             0x02 // CONTINUOUS MEASUREMENT MODE1 10Hz
#define AK09940_MODE_CONT2             0x04 // CONTINUOUS MEASUREMENT MODE2 20Hz
#define AK09940_MODE_CONT3             0x06 // CONTINUOUS MEASUREMENT MODE3 50Hz
#define AK09940_MODE_CONT4             0x08 // CONTINUOUS MEASUREMENT MODE4 100Hz
#define AK09940_MODE_CONT5             0x0A // CONTINUOUS MEASUREMENT MODE5 200Hz
#define AK09940_MODE_CONT6             0x0C // CONTINUOUS MEASUREMENT MODE6 400Hz
#define AK09940_MODE_SELF_TEST         0x10 // SELF TEST MODE

#define AK09940_VAL_WIA1               0x48
#define AK09940_VAL_WIA2               0xA0
#define AK09940_VAL_SOFT_RESET         0x01
#define AK09940_VAL_I2CDIS             0x1B

#define AK09940_BIT_MASK_DRDY          0x01
#define AK09940_BIT_MASK_DOR           0x01
#define AK09940_BIT_MASK_INV           0x02
#define AK09940_BIT_MASK_TEM           0x40
#define AK09940_BIT_MASK_MT            0x60
#define AK09940_BIT_MASK_MODE          0x1F

#define AK09940_BIT_OFFSET_MT          5
#define AK09940_BIT_OFFSET_TEM         6

#define AK09940_WAIT_POWER_DOWN_US     100
#define AK09940_WAIT_SOFT_RESET_US     1000

#define AK09940_SENSITIVITY            (0.01)    // uT/LSB
#define AK09940_TLIMIT_MIN_X           -200      // TBD
#define AK09940_TLIMIT_MIN_Y           -200      // TBD
#define AK09940_TLIMIT_MIN_Z           -1000     // TBD
#define AK09940_TLIMIT_MAX_X           200       // TBD
#define AK09940_TLIMIT_MAX_Y           200       // TBD
#define AK09940_TLIMIT_MAX_Z           -150      // TBD
#define AK09940_LEN_DATA                12

/**
 * This is a device driver for the AK09940 3-axis magnetometer by AKM Semiconductor, Inc.
 *
 * @note AK09940 is a 3-axis magnetometer (magnetic sensor) device manufactured by AKM.
 * 
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak09940.h"
 * 
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 * 
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // Creates an instance of AK09940
 *     AK09940 ak09940(&connection, AK09940::SLAVE_ADDR_1);
 *     
 *     // Checks connectivity
 *     if(ak09940.checkConnection() != AK09940::SUCCESS) {
 *         // Failed to check device connection.
 *         // - error handling here -
 *     }
 *     
 *     // Puts the device into continuous measurement mode.
 *     AkmECompass::Mode mode;
 *     mode.mode = AK09940::MODE_CONTINUOUS_1;
 *     if(ak09940.setOperationMode(mode) != AK09940::SUCCESS) {
 *         // Failed to set the device into continuous measurement mode.
 *         // - error handling here -
 *     }
 *
 *     while(true) {
 *         // checks DRDY
 *         if (statusAK09940 == AK09940::DATA_READY) {
 *             AK09940::MagneticVector mag;
 *             ak09940.getMagneticVector(&mag);
 *             // You may use serial output to see data.
 *             // serial.printf("%5.1f,%5.1f,%5.1f\n",
 *             //    mag.mx, mag.my, mag.mz);
 *             statusAK09940 = AK09940::NOT_DATA_READY;
 *         } else if (statusAK09940 == AK09940::NOT_DATA_READY) {
 *             // Nothing to do.
 *         } else {
 *             // - error handling here -
 *         }
 *     }
 * }
 * @endcode
 */
class AK09940 : public AkmECompass
{
public:
    
    /**
     * Constructor.
     *
     */
    AK09940();

    /**
     * Destructor.
     *
     */
    virtual ~AK09940();
    
    /**
     * Initialize sensor with I2C connection.
     *
     * @param _i2c Pointer to the I2C instance
     * @param addr Slave address of the device
     * @param deviceId Device Id enum
     *
     */
    virtual void init(I2C *_i2c, SlaveAddress addr, DeviceId deviceId);

    /**
     * Initialize sensor with SPI connection.
     *
     * @param _spi Pointer to the SPI instance
     * @param _cs Pointer to the chip select DigitalOut pin
     * @param deviceId Device Id enum
     *
     */
    virtual void init(SPI *_spi, DigitalOut *_cs, DeviceId deviceId);
    
    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status checkConnection();

    /**
     * Gets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getOperationMode(Mode *mode);

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status setOperationMode(Mode mode);

    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    virtual AkmECompass::Status isDataReady();

    /**
     * Gets magnetic vector from the device in LSB.
     *
     * @param lsb Pointer to a instance of MagneticVectorLsb
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getMagneticVectorLsb(MagneticVectorLsb *lsb);

    /**
     * Gets magnetic vector from the device in uT.
     *
     * @param vec Pointer to a instance of MagneticVector
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getMagneticVector(MagneticVector *vec);
      
private:

    /**
     * Gets magnetic data, from the register ST1 (0x02) to ST2 (0x09), from the device.
     * 
     * @param buf buffer to store the data read from the device.
     * 
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    AkmECompass::Status getData(char *buf);
        
    /**
     * Sub init function.
     *
     */
    void init_sub();
};

#endif
