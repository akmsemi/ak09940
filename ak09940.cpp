#include "ak09940.h"


AK09940::AK09940(){
    i2c = NULL;
    spi = NULL;
    cs = NULL;
}

AK09940::~AK09940(){
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
}

void AK09940::init_sub() {
    // software reset
    char buf = AK09940_VAL_SOFT_RESET;
    AK09940::write(AK09940_REG_CNTL4, &buf, AKMECOMPASS_LEN_ONE_BYTE);
    wait_us(AK09940_WAIT_SOFT_RESET_US);
}

void AK09940::init(I2C *_i2c, SlaveAddress addr, DeviceId id) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    i2c = _i2c;
    slaveAddress = addr;    
    deviceId = id;
    
    init_sub();
}

void AK09940::init(SPI *_spi, DigitalOut *_cs, DeviceId id) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    spi=_spi;
    cs=_cs;
    cs->write(1);
    deviceId = id;

    init_sub();

    // disable I2C
    char buf = AK09940_VAL_I2CDIS;
    AK09940::write(AK09940_REG_I2CDIS, &buf, AKMECOMPASS_LEN_ONE_BYTE);
}

AkmECompass::Status AK09940::checkConnection() {
    AkmECompass::Status status = AkmECompass::SUCCESS;

    // Gets the WIA1 register value.
    char wia1Value = 0;
    if ((status=AK09940::read(AK09940_REG_WIA1, &wia1Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        return status;
    }

    // Checks the obtained value equals to the supposed value.
    if (wia1Value != AK09940_VAL_WIA1) {
        return AkmECompass::ERROR;
    }

    // Gets the WIA2 register value.
    char wia2Value = 0;
    if ((status=AK09940::read(AK09940_REG_WIA2, &wia2Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        return status;
    }
    // Checks the obtained value equals to the supposed value.
    if ( wia2Value != deviceId ) {
        return AkmECompass::ERROR;
    }
    
    return status;
}


AkmECompass::Status AK09940::isDataReady() {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    // Gets the ST1 register value.
    char st1Value = 0;
    if ((status=AK09940::read(AK09940_REG_ST1, &st1Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }

    // Sets a return status corresponds to the obtained value.    
    if ((st1Value & AK09940_BIT_MASK_DRDY) > 0) {
        status = AkmECompass::DATA_READY;
    } else {
        status = AkmECompass::NOT_DATA_READY;
    }
    return status;
}

AkmECompass::Status AK09940::getData(char *buf) {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    if ((status=AK09940::read(AK09940_REG_ST1, buf, AK09940_LEN_DATA)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    return status;
}

AkmECompass::Status AK09940::getOperationMode(AkmECompass::Mode *mode){
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf = 0;

    if ((status=AK09940::read(AK09940_REG_CNTL3, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    mode->mode = (AkmECompass::OperationMode)(buf & AK09940_BIT_MASK_MODE);
    mode->options[0] = (buf >> AK09940_BIT_OFFSET_MT) & 0x03;
    
    buf = 0;
    if ((status=AK09940::read(AK09940_REG_CNTL2, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    mode->options[1] = (buf >> AK09940_BIT_OFFSET_TEM) & 0x01;
    
    return status; 
}

AkmECompass::Status AK09940::setOperationMode(AkmECompass::Mode mode){
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf[2];
    buf[0] = AkmECompass::MODE_POWER_DOWN;

    // The device has to be put into power-down mode first before switching mode.
    if (mode.mode != AkmECompass::MODE_SINGLE_LOOP && mode.mode != AkmECompass::MODE_POWER_DOWN) {
        if ((status=AK09940::write(AK09940_REG_CNTL3, buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
            // I2C write failed.
            return status;
        }
        wait_us(AK09940_WAIT_POWER_DOWN_US);
    }

    // Set CNTL2 Register
    buf[0] = (mode.options[1]<<AK09940_BIT_OFFSET_TEM) & AK09940_BIT_MASK_TEM;

    if(mode.mode == AkmECompass::MODE_SINGLE_LOOP)
    {
        // Switch to the specified mode
        // ignore CTRL2 write for faster sampling
        buf[0] = AkmECompass::MODE_SINGLE_MEASUREMENT | (mode.options[0]<<AK09940_BIT_OFFSET_MT);
        if ((status=AK09940::write(AK09940_REG_CNTL3, buf, 1)) != AkmECompass::SUCCESS) {
            // I2C write failed.
            return status;
        }
    }
    else
    {
      // Switch to the specified mode
      buf[1] = mode.mode | (mode.options[0]<<AK09940_BIT_OFFSET_MT);
      if ((status=AK09940::write(AK09940_REG_CNTL2, buf, 2)) != AkmECompass::SUCCESS) {
          // I2C write failed.
          return status;
      }
    }
    return AkmECompass::SUCCESS;
}

AkmECompass::Status AK09940::getMagneticVectorLsb(MagneticVectorLsb *lsb) {
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf[AK09940_LEN_DATA];
    
    if ((status=AK09940::getData(buf)) != AkmECompass::SUCCESS) {
        // Failed to get data.
        return status;
    }

    uint32_t polX = (buf[3]&0x02) ? 0xFFFC0000 : 0x00000000;
    uint32_t polY = (buf[6]&0x02) ? 0xFFFC0000 : 0x00000000;
    uint32_t polZ = (buf[9]&0x02) ? 0xFFFC0000 : 0x00000000;
    lsb->lsbX = (int32_t)( polX | (uint32_t)(buf[3]<<16 & 0x00030000) | (uint32_t)(buf[2]<<8 & 0x0000FF00) | (uint32_t)(buf[1] & 0x000000FF) );
    lsb->lsbY = (int32_t)( polY | (uint32_t)(buf[6]<<16 & 0x00030000) | (uint32_t)(buf[5]<<8 & 0x0000FF00) | (uint32_t)(buf[4] & 0x000000FF) );
    lsb->lsbZ = (int32_t)( polZ | (uint32_t)(buf[9]<<16 & 0x00030000) | (uint32_t)(buf[8]<<8 & 0x0000FF00) | (uint32_t)(buf[7] & 0x000000FF) );
    lsb->lsbTemp = (int8_t)buf[10];

    return AkmECompass::SUCCESS;
}

AkmECompass::Status AK09940::getMagneticVector(MagneticVector *vec) {
    AkmECompass::Status status = AkmECompass::ERROR;
    MagneticVectorLsb lsb;
    status = getMagneticVectorLsb(&lsb);
    if(status == AkmECompass::SUCCESS){
        vec->isOverflow = lsb.isOverflow;
        vec->mx = (float)( lsb.lsbX * AK09940_SENSITIVITY );
        vec->my = (float)( lsb.lsbY * AK09940_SENSITIVITY );
        vec->mz = (float)( lsb.lsbZ * AK09940_SENSITIVITY );
        vec->temp = (float)( 30.0 - (lsb.lsbTemp)/1.6 );
    }else{
        return status;
    }
    return AkmECompass::SUCCESS;
}
